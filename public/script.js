function calculator(){
    let a = document.getElementById("stoimost");
    let b = document.getElementById("colichestvo");
    var c=true;
    if ((!b.value.match(/^\d+$/))||(!a.value.match(/^\d+$/))) c=false;
    if (c) {
        let r = document.getElementById("result");
        r.innerHTML = a.value * b.value;
    } 
	else {
            alert('Введите корректное значение');
            a.value = ''; b.value = '';
        r.innerHTML = 0;
    }

}

window.addEventListener('DOMContentLoaded', function(event) {
    console.log("DOM fully loaded and parsed");
    let h = document.getElementById("button");
    h.addEventListener("click", calculator);
});
